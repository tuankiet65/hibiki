#!/usr/bin/env python3

import logging
import pprint
import sys
from datetime import datetime
from pathlib import Path

from hibiki import transcoders
from hibiki.database import LocalDatabase, RemoteDatabase
from hibiki.config import Config
from hibiki.syncer import Syncer


def get_logfile_name() -> str:
    time = datetime.now()
    time_str = time.strftime("%Y%m%d-%H%M%S")
    return f"hibiki-{time_str}.log"


def get_logger() -> logging.Logger:
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter(
        "[%(asctime)s][%(levelname)s][%(filename)s:%(lineno)d] %(message)s")

    stdout_handler = logging.StreamHandler(sys.stdout)
    stdout_handler.setFormatter(formatter)
    # logfile_handler = logging.FileHandler(get_logfile_name(), mode = "w")
    # logfile_handler.setFormatter(formatter)

    logger.addHandler(stdout_handler)
    # logger.addHandler(logfile_handler)

    return logger


def main() -> None:
    logger = get_logger()

    logger.info("Hibiki started")

    config_file = Path(sys.argv[1])
    config = Config.get(config_file)

    logger.debug("Config:\n %s", pprint.pformat(config, indent=4))

    if config['transcode']['enabled']:
        transcoder = transcoders.get(config['transcode']['transcoder'])
        transcoder = transcoder(
            config['transcode']['bitrate'],
            config['transcode']['bitrate_mode'])
    else:
        transcoder = transcoders.get("null")()

    local_collection = LocalDatabase()
    for path in config['collection']['local']:
        local_collection.add_folder(Path(path))

    for path in config['collection']['remote']:
        folder = Path(path)
        if not folder.exists():
            folder.mkdir()

        remote_collection = RemoteDatabase(folder, transcoder)
        syncer = Syncer(
            local_collection, remote_collection,
            max_threads = config['general']['threads'],
            autosave_interval = config['general']['autosave_interval'])
        syncer.sync()


if __name__ == "__main__":
    main()
