#!/usr/bin/env python3

import unittest
from pathlib import Path
from typing import Callable

import ffmpeg

from hibiki import utils
from hibiki.transcoders import *


class TranscoderTest(unittest.TestCase):
    source = \
        Path(
            "data/Ocean of Blossoms/Frozen Starfall - Ocean of Blossoms - 01 Sunlight Diamonds (FS Remix feat. Muffin).flac")

    def setUp(self):
        assert self.source.exists(), "source does not exist"

    def __do_transcode(self, transcoder: Transcoder, destination: Path,
                       expected_codec: str, is_expected_bitrate: Callable):
        transcoder.transcode(self.source, destination)
        assert destination.exists(), f"destination file ({destination}) does not exist"

        probe = ffmpeg.probe(str(destination))
        stream_count = probe['format']['nb_streams']
        assert stream_count == 1, f"more than 1 streams in transcoded file (got {stream_count})"

        audio_stream = probe['streams'][0]
        codec_type = audio_stream['codec_type']
        assert codec_type == "audio", f"codec_type is not 'audio' (got '{codec_type}')"

        codec = audio_stream['codec_name']
        assert codec == expected_codec, f"codec_name does not match (expected '{expected_codec}', got '{codec}')"

        try:
            bitrate = int(audio_stream['bit_rate'])
        except KeyError:
            bitrate = int(probe['format']['bit_rate'])
        assert is_expected_bitrate(bitrate), f"bitrate does not satisfy condition (got {bitrate})"

        destination.unlink()

    def test_opus(self):
        transcoder = FFmpegOpusTranscoder(
            bitrate_mode=BitrateMode.VBR,
            bitrate=128
        )
        destination = utils.make_temp_file(transcoder.file_extension)
        self.__do_transcode(transcoder, destination,
                            "opus", lambda bitrate: 120000 <= bitrate <= 140000)

    def test_lame_320k(self):
        transcoder = FFmpegLameTranscoder(
            bitrate_mode=BitrateMode.CBR,
            bitrate=320
        )
        destination = utils.make_temp_file(transcoder.file_extension)
        self.__do_transcode(transcoder, destination,
                            "mp3", lambda bitrate: bitrate == 320000)

    def test_lame_v0(self):
        transcoder = FFmpegLameTranscoder(
            bitrate_mode=BitrateMode.VBR,
            bitrate=0
        )
        destination = utils.make_temp_file(transcoder.file_extension)
        self.__do_transcode(transcoder, destination,
                            "mp3", lambda bitrate: 250000 <= bitrate <= 260000)

    def test_aac_128k(self):
        transcoder = FFmpegNativeAacTranscoder(
            bitrate_mode=BitrateMode.CBR,
            bitrate=128
        )
        destination = utils.make_temp_file(transcoder.file_extension)
        self.__do_transcode(transcoder, destination,
                            "aac", lambda bitrate: 127000 <= bitrate <= 129000)

    def test_vorbis_128k(self):
        transcoder = FFmpegVorbisTranscoder(
            bitrate_mode=BitrateMode.CBR,
            bitrate=128
        )
        destination = utils.make_temp_file(transcoder.file_extension)
        self.__do_transcode(transcoder, destination,
                            "vorbis", lambda bitrate: bitrate == 128000)

    def test_vorbis_q5(self):
        transcoder = FFmpegVorbisTranscoder(
            bitrate_mode=BitrateMode.VBR,
            bitrate=5.0
        )
        destination = utils.make_temp_file(transcoder.file_extension)
        self.__do_transcode(transcoder, destination,
                            "vorbis", lambda bitrate: bitrate == 160000)
