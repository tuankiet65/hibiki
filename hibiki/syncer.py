#!/usr/bin/env python3

import logging
import os
import threading
import time
from concurrent.futures import ThreadPoolExecutor

from hibiki.database import LocalDatabase, RemoteDatabase

logger = logging.getLogger(__name__)


class Syncer:
    def __init__(self,
                 local_database: LocalDatabase,
                 remote_database: RemoteDatabase,
                 max_threads: int = -1,
                 autosave_interval: int = 10):
        self.__local_database = local_database
        self.__remote_database = remote_database

        self.__max_threads = max_threads
        if self.__max_threads == -1:
            # if the system has only one CPU available, then
            # (os.cpu_count() - 1) == 0, which is False
            # 1 would then be assigned
            cpu_count = os.cpu_count() or 1
            self.__max_threads = (cpu_count - 1) or 1

        self.__autosave_interval = autosave_interval
        self.__autosave_enabled = False

    def sync(self) -> None:
        local_track_ids = set(self.__local_database.tracks.keys())
        remote_track_ids = set(self.__remote_database.tracks.keys())

        local_to_remote_diff = local_track_ids - remote_track_ids
        remote_to_local_diff = remote_track_ids - local_track_ids

        logger.info("Local - remote = %d tracks", len(local_to_remote_diff))
        logger.info("Remote - local = %d tracks", len(remote_to_local_diff))

        logger.info("Removing all remote tracks not present on local "
                    "collection")

        # spawn a thread to autosave self.__remote_collection at predefined
        # intervals
        self.__autosave_enabled = True
        autosave_thread = threading.Thread(
            target=self.__autosave_remote_thread,
            name="hibiki_autosave")
        autosave_thread.start()

        thread_pool = ThreadPoolExecutor(max_workers=self.__max_threads,
                                         thread_name_prefix="hibiki_")

        # remove all tracks present in remote collection but not local
        for track_id in remote_to_local_diff:
            self.__remote_database.remove_track(track_id)
            #thread_pool.submit(self.__remote_collection.remove_track, track_id)

        # add all tracks present in local collection but not in remote
        logger.info("Adding all local tracks not present on remote collection")
        for track_id in local_to_remote_diff:
            track = self.__local_database.tracks[track_id]
            # thread_pool.submit(self.__remote_collection.add_track, track)
            self.__remote_database.add_track(track)

        # wait for all operations to complete
        thread_pool.shutdown()

        # wait for the autosave thread to exit gracefully
        self.__autosave_enabled = False
        autosave_thread.join()

    def __autosave_remote_thread(self) -> None:
        while self.__autosave_enabled:
            time.sleep(self.__autosave_interval)
            self.__remote_database.save()
