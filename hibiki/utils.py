#!/usr/bin/env python3

import os
import hashlib
import tempfile
import string
import unidecode

from pathlib import Path

PARTIAL_HASH_PORTION = 32768  # 32 kB


def get_file_partial_hash(file_path: Path) -> str:
    """
    Return MD5 hash for the first PARTIAL_HASH_PORTION bytes of file_path

    :param file_path: Path to file
    :return: MD5 hash, in hexadecimal string
    """

    with file_path.open("rb") as f:
        buffer = f.read(PARTIAL_HASH_PORTION)

    return hashlib.md5(buffer).hexdigest()


def get_str_md5(s: str) -> str:
    """

    :param s:
    :return:
    """

    buffer = s.encode("utf-8")
    return hashlib.md5(buffer).hexdigest()


def make_temp_file(extension: str) -> Path:
    """

    :param extension:
    :return:
    """
    assert extension.startswith(".")
    temp_file_fd, temp_file_name = tempfile.mkstemp(extension)
    os.close(temp_file_fd)
    return Path(temp_file_name)


def sanitize_path_name(path: str) -> str:
    """

    :param path:
    :return:
    """
    ALLOWED_CHARACTERS = string.ascii_letters + string.digits + " !.-_+[]()"

    # Convert Unicode character to ASCII (This need to be done first)
    # It doesn't and needn't be accurate
    uni_decoded = unidecode.unidecode(path)

    result = ""
    # Illegal characters
    for ch in uni_decoded:
        if ch in ALLOWED_CHARACTERS:
            result += ch
        else:
            result += f"_x{ord(ch):02x}_"

    # Folder names with trailing spaces like 'e.p.' will become 'e.p'
    # We'd strip the trailing dot
    result = result.strip('.')

    # Creating a directory with trailing space on Linux triggers
    # "Invalid argument"
    result = result.strip()

    return result
