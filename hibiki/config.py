#!/usr/bin/env python3

import logging
from pathlib import Path

import toml
from schema import Optional, Schema, SchemaError, Or

logger = logging.getLogger(__name__)

SCHEMA = Schema({
    "general": {
        Optional("threads", -1): lambda core: core == -1 or core > 0,
        Optional("autosave_interval", 5): lambda interval: interval > 0,
        Optional("dry_run", False): bool
    },
    "collection": {
        "local": list,
        "remote": list,
    },
    "transcode": {
        Optional("enabled", True): bool,
        Optional("transcoder", "ffmpeg/opus"): str,
        Optional("bitrate_mode", "vbr"): str,
        Optional("bitrate", "128"): Or(int, float)
    },
    "resize": {
        Optional("enabled", True): bool,
        Optional("engine", "auto"): str,
        Optional("minimum_width", 1920): int,
        Optional("minimum_height", -1): int
    }
})


class Config:
    """

    """

    @staticmethod
    def get(config_file: Path):
        """
        Parse config_file and return a config dictionary. On error, raises
        exception. Config file are formatted according to the schema defined
        above.

        :param config_file: path to config file
        :return: Config dictionary that follows the defined schema
        """
        logger.info("Reading config file at %s", str(config_file))
        try:
            with config_file.open() as f:
                raw_config = toml.load(f)
            config = SCHEMA.validate(raw_config)
        except toml.TomlDecodeError:
            logger.exception("Unable to decode TOML config file")
        except SchemaError:
            logger.exception("TOML config file does not match config schema")
        else:
            return config
