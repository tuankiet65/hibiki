#!/usr/bin/env python3

import logging
from pathlib import Path
from typing import Any, Dict, List

from PIL import Image
from tinytag import TinyTag, TinyTagException

from hibiki import utils

logger = logging.getLogger(__name__)


class CoverArt:
    def __init__(self, id: str, file_path: Path = None,
                 width: int = None, height: int = None):
        self.__id = id
        self.__file_path = file_path
        self.__width = width
        self.__height = height

    @classmethod
    def load_file(cls, file_path: Path):
        """

        :param file_path:
        """

        cover_info = dict()
        cover_info["file_path"] = file_path

        try:
            img = Image.open(file_path)
            cover_info["width"], cover_info["height"] = img.size
        except IOError as e:
            # TODO: properly handle this exception
            raise ImageFormatNotSupported

        cover_info["id"] = utils.get_file_partial_hash(file_path)

        return cls(**cover_info)

    def as_dict(self, include_path: bool = False) -> Dict[str, Any]:
        """

        :param include_path:
        :return:
        """
        result = {
            "id": self.id,
            "width": self.width,
            "height": self.height
        }

        if include_path:
            result["file_path"] = str(self.file_path)

        return result

    @classmethod
    def from_dict(cls, dictionary: Dict[str, Any]):
        """

        :param dictionary:
        :return:
        """

        if "file_path" in dictionary:
            dictionary["file_path"] = Path(dictionary["file_path"])
        return cls(**dictionary)

    @property
    def file_path(self):
        return self.__file_path

    @property
    def id(self):
        return self.__id

    @property
    def width(self):
        return self.__width

    @property
    def height(self):
        return self.__height

    def __eq__(self, other) -> bool:
        """

        :param other:
        :return:
        """
        if not isinstance(other, CoverArt):
            return False
        return self.file_path == other.file_path


class Track:
    """
    Class representing a track, or more formally, an audio file on disk.
    """

    COVER_ART_FILENAMES = [
        "cover.png",
        "cover.jpg",
        "cover.jpeg",
        "folder.png",
        "folder.jpg",
        "folder.jpeg",
        "front.png",
        "front.jpg",
        "front.jpeg"
    ]

    def __init__(self, id: str,
                 cover_art: CoverArt = None, file_path: Path = None,
                 title: str = None, album_title: str = None,
                 track_num: int = None, disc: int = None, year: int = None,
                 remote_path: Path = None, remote_hash: str = None):
        """

        :param id:
        :param cover_art:
        :param file_path:
        :param title:
        :param album_title:
        :param track_num:
        :param disc:
        :param year:
        """
        self.__id = id
        self.__cover_art = cover_art
        self.__file_path = file_path
        self.__title = title
        self.__album_title = album_title
        self.__track_num = track_num
        self.__disc = disc
        self.__year = year
        self.__remote_path = remote_path
        self.__remote_hash = remote_hash

    @classmethod
    def load_file(cls, file_path: Path):
        """

        :param file_path:
        """
        track_info = dict()

        if not TinyTag.is_supported(str(file_path)):
            # TODO: fallback to ffprobe?
            raise AudioFormatNotSupported
        try:
            metadata = TinyTag.get(str(file_path))
            track_info["title"] = metadata.title

            try:
                track_info["track_num"] = int(metadata.track)
            except (TypeError, ValueError):
                logger.warning("File has invalid track number, defaulting to "
                               "1: %s", str(file_path))
                track_info["track_num"] = 1

            try:
                track_info["disc"] = int(metadata.disc)
            except (TypeError, ValueError):
                logger.warning("File has invalid disc, defaulting to 1: %s",
                               str(file_path))
                track_info["disc"] = 1

            track_info["album_title"] = metadata.album

            try:
                track_info["year"] = int(metadata.year)
            except (TypeError, ValueError):
                logger.warning("File has invalid year, defaulting to 1: %s",
                               str(file_path))
                track_info["year"] = 0
        except TinyTagException as _:
            # TODO: Properly handle this exception
            raise AudioFormatNotSupported

        for cover_art_filename in cls.COVER_ART_FILENAMES:
            cover_art_path = file_path.parent / cover_art_filename
            if cover_art_path.exists():
                track_info["cover_art"] = CoverArt.load_file(cover_art_path)
                break

        track_info["id"] = utils.get_file_partial_hash(file_path)
        track_info["file_path"] = file_path

        return cls(**track_info)

    def as_dict(self, include_path: bool = False, include_remote: bool = False):
        """

        :param include_path:
        :param include_remote:
        :return:
        """
        result = {
            "id": self.id,
            "title": self.title,
            "album_title": self.album_title,
            "year": self.year,
            "track_num": self.track_num,
            "disc": self.disc
        }

        if self.__cover_art is not None:
            result["cover_art"] = self.__cover_art.as_dict(include_path)

        if include_path:
            result["file_path"] = str(self.file_path)

        if include_remote:
            result["remote_path"] = str(self.remote_path)
            result["remote_hash"] = self.remote_hash

        return result

    @classmethod
    def from_dict(cls, dictionary: Dict[str, Any]):
        """

        :param dictionary:
        :return:
        """

        if "cover_art" in dictionary:
            dictionary["cover_art"] = CoverArt.from_dict(dictionary["cover_art"])
        if "file_path" in dictionary:
            dictionary["file_path"] = Path(dictionary["file_path"])
        if "remote_path" in dictionary:
            dictionary["remote_path"] = Path(dictionary["remote_path"])

        return cls(**dictionary)

    def add_remote_info(self, remote_path: Path, remote_parent_dir: Path):
        self.__remote_path = remote_path.relative_to(remote_parent_dir)
        self.__remote_hash = utils.get_file_partial_hash(remote_path)

    def verify_remote_integrity(self, remote_parent_dir: Path):
        real_remote_path = remote_parent_dir / self.remote_path

        if not real_remote_path.exists():
            return False

        return \
            utils.get_file_partial_hash(real_remote_path) == self.__remote_hash

    @property
    def id(self):
        return self.__id

    @property
    def cover_art(self):
        return self.__cover_art

    @property
    def file_path(self):
        return self.__file_path

    @property
    def title(self):
        return self.__title

    @property
    def album_title(self):
        return self.__album_title

    @property
    def year(self):
        return self.__year

    @property
    def album_id(self):
        return utils.get_str_md5(f"{self.__year}/{self.__album_title}")

    @property
    def track_num(self):
        return self.__track_num

    @property
    def disc(self):
        return self.__disc

    @property
    def remote_path(self):
        return self.__remote_path

    @property
    def remote_hash(self):
        return self.__remote_hash


class Album:
    """
    Class representing an album, or more formally, a collection of tracks and
    a cover art
    """

    def __init__(self, title: str, tracks: List[Track] = None):
        """

        """
        self.__title = title

        self.__tracks = []
        if tracks:
            for track in tracks:
                self.add_track(track)

    @classmethod
    def from_dict(cls, dictionary: Dict):
        new_dict = dict()

        new_dict["title"] = dictionary["title"]
        new_dict["tracks"] = list()
        for track_dict in dictionary["tracks"]:
            new_dict["tracks"].append(Track.from_dict(track_dict))

        return cls(**new_dict)

    def as_dict(self,
                include_path: bool = False,
                include_remote: bool = False) -> Dict[str, Any]:
        """
        Converting self into a dictionary suitable for use with JSON/YAML

        :return: Dictionary representation of self
        """
        result = {
            "title": self.title,
            "tracks": [track.as_dict(include_path,
                                     include_remote) for track in self.tracks],
        }

        return result

    def add_track(self, track: Track) -> None:
        """
        Add a track to the album
        :param track: Track to be added
        """

        self.__tracks.append(track)

    @property
    def title(self) -> str:
        return self.__title

    @property
    def tracks(self) -> List[Track]:
        return self.__tracks

    @property
    def cover_art(self) -> CoverArt or None:
        """
        Return the cover art of the album. If all tracks in the album have
        the same cover art, then this is considered the cover art of the album
        and returned. Otherwise, None is returned.

        :return: CoverArt, or None
        """
        all_cover_arts = [track.cover_art for track in self.__tracks]

        # in case there're no tracks yet so all_cover_arts is empty
        if not all_cover_arts:
            return None
        elif None in all_cover_arts:
            return None
        else:
            comparison_result = \
                [cover_art == all_cover_arts[0] for cover_art in all_cover_arts]
            if all(comparison_result):
                return all_cover_arts[0]
            else:
                return None


class ImageFormatNotSupported(Exception):
    pass


class AudioFormatNotSupported(Exception):
    pass
