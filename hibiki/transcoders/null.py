import shutil
from pathlib import Path

from hibiki.transcoders import Transcoder, TranscodingError


class NullTranscoder(Transcoder):
    """
    This transcoder does not perform any transcoding operations, rather it just
    copies file from src to dst
    """

    def __init__(self):
        super().__init__()

    def transcode(self, src: Path, dst: Path) -> Path:
        try:
            shutil.copyfile(str(src), str(dst))
        except shutil.SameFileError as e:
            # TODO
            raise TranscodingError
        except IOError as e:
            # TODO
            raise TranscodingError

        return dst

    @property
    def file_extension(self) -> str:
        # this property is not supposed to be called
        raise NotImplementedError
