from typing import Dict, Type

from .base import BitrateMode, Transcoder, TranscodingError
from .ffmpeg import FFmpegLameTranscoder, FFmpegNativeAacTranscoder, \
    FFmpegOpusTranscoder, FFmpegTranscoder, FFmpegVorbisTranscoder
from .null import NullTranscoder

__all__ = [
    'Transcoder',
    'TranscodingError',
    'BitrateMode',
    'FFmpegTranscoder',
    'FFmpegOpusTranscoder',
    'FFmpegNativeAacTranscoder',
    'FFmpegLameTranscoder',
    'FFmpegVorbisTranscoder',
    'NullTranscoder'
]

import logging

logger = logging.getLogger(__name__)


def get(name: str) -> Type[Transcoder]:
    """
    Return a Transcoder matching the name specified. Raises exception
    if no transcoder with the requested name is available. Note that the caller
    is responsible for the initialization of the returning Transcoder.

    :param name: transcoder name as a string
    :return: the appropriate Transcoder
    """
    available_trancoders: Dict[str, Type[Transcoder]] = {
        "ffmpeg/opus": FFmpegOpusTranscoder,
        "ffmpeg/aac": FFmpegNativeAacTranscoder,
        "ffmpeg/lame": FFmpegLameTranscoder,
        "ffmpeg/vorbis": FFmpegVorbisTranscoder,
        "null": NullTranscoder
    }

    try:
        return available_trancoders[name]
    except KeyError:
        logger.exception("No available transcoder named '%s'", name)
