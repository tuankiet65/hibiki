import abc
import enum
import logging
from pathlib import Path

logger = logging.getLogger(__name__)


class BitrateMode(enum.Enum):
    CBR = "cbr"
    VBR = "vbr"


class TranscodingError(Exception):
    """

    """
    def __init__(self, error: str):
        super().__init__(f'Transcoding error')
        self.error = error



class Transcoder(abc.ABC):
    """
    Abstract class for representing a transcoder. This class does nothing,
    use a subclass instead (eg: FFmpegOpusTranscoder, FFmpegMp3Transcoder, ...)
    """

    @abc.abstractmethod
    def __init__(self, *args, **kwargs):
        """
        Abstract method for initializing the class.

        :param args:
        :param kwargs:
        """
        pass

    @abc.abstractmethod
    def transcode(self, src: Path, dst: Path) -> Path:
        """
        Abstract method for performing transcoding operations.
        Blocks until either the operation is finished or TranscodingError is
        raised.

        :param src: path to source file
        :param dst: destination path
        :return: dst
        """
        pass

    @property
    @abc.abstractmethod
    def file_extension(self) -> str:
        """
        Abstract class for getting the file extension of a transcoded file
        :return:
        """
        pass
