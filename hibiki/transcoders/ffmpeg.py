#!/usr/bin/env python3

import abc
import logging
import shutil
import subprocess
from pathlib import Path
from typing import Dict, List, Optional

# noinspection PyPackageRequirements
import ffmpeg

from hibiki import utils
from hibiki.transcoders import BitrateMode, Transcoder, TranscodingError

logger = logging.getLogger(__name__)


class FFmpegTranscoder(Transcoder, abc.ABC):
    _encoder: str
    _file_extension: str

    def __init__(self,
                 bitrate: float = 128,
                 bitrate_mode: BitrateMode = BitrateMode.VBR,
                 ffmpeg_path: Path = None):
        """

        :param ffmpeg_path: Path to FFmpeg executable
        :param bitrate_mode: Bitrate mode, can be
        BitrateMode.CBR (corresponds to hard-cbr) or BitrateMode.VBR
        :param bitrate: Bitrate, expresses in kilobits
        """
        assert self._encoder is not None
        assert self._file_extension is not None

        super().__init__()

        self.__ffmpeg_path = ffmpeg_path
        if self.__ffmpeg_path is None:
            self.__ffmpeg_path = autodetect_ffmpeg_path(self._encoder)
        self.bitrate_mode = bitrate_mode
        self.bitrate = bitrate

        if self.__ffmpeg_path is None:
            raise FFmpegNotFoundException
        self.verify_quality()

        logging.info("Transcoder initialized, bitrate = %d, bitrate mode = %s",
                     self.bitrate, self.bitrate_mode)

    @abc.abstractmethod
    def verify_quality(self) -> None:
        """

        :return:
        """
        raise NotImplementedError

    @abc.abstractmethod
    def get_quality_args(self) -> Dict[str, str]:
        """

        :return:
        """
        raise NotImplementedError

    def transcode(self, src: Path, dst: Path) -> Path:
        """

        :param src:
        :param dst:
        :return:
        """

        temp_file = utils.make_temp_file(self.file_extension)

        ffmpeg_args = {
            "map": "a",
            "acodec": self._encoder
        }
        ffmpeg_args.update(self.get_quality_args())

        stream = ffmpeg.input(str(src))
        stream = ffmpeg.output(stream, str(temp_file), **ffmpeg_args)
        stream = ffmpeg.overwrite_output(stream)
        logger.debug("FFmpeg command: %s", ffmpeg.compile(stream))

        try:
            _, stderr = ffmpeg.run(stream,
                                   cmd=str(self.__ffmpeg_path),
                                   capture_stderr=True)
            shutil.move(temp_file, dst)
        except ffmpeg.Error as e:
            error = f"stdout: {e.stdout}\nstderr: {e.stderr}"
            raise TranscodingError(error)

        return dst

    @property
    def encoder(self) -> str:
        return self._encoder

    @property
    def file_extension(self) -> str:
        return self._file_extension


class FFmpegOpusTranscoder(FFmpegTranscoder):
    _encoder = "libopus"
    _file_extension = ".opus"

    def verify_quality(self) -> None:
        # Bitrate range taken from
        # https://opus-codec.org/docs/opus_api-1.2/index.html
        if not (6 <= self.bitrate <= 512):
            raise InvalidSettings("Bitrate must be between 6kbps and 512kbps")

    def get_quality_args(self) -> Dict[str, str]:
        return {
            "audio_bitrate": f"{self.bitrate}k",
            "vbr": "on" if (self.bitrate_mode == BitrateMode.VBR) else "off"
        }


class FFmpegLameTranscoder(FFmpegTranscoder):
    _encoder = "libmp3lame"
    _file_extension = ".mp3"

    def verify_quality(self) -> None:
        if self.bitrate_mode == BitrateMode.VBR:
            if not (0.0 <= self.bitrate <= 10.0):
                raise InvalidSettings(
                    "LAME quality factor must be between 0.0 and 10.0")

    def get_quality_args(self) -> Dict[str, str]:
        if self.bitrate_mode == BitrateMode.CBR:
            return {
                "audio_bitrate": f"{self.bitrate}k"
            }
        else:
            return {
                "aq": f"{self.bitrate}"
            }


class FFmpegNativeAacTranscoder(FFmpegTranscoder):
    _encoder = "aac"
    _file_extension = ".m4a"

    def verify_quality(self) -> None:
        if self.bitrate_mode == BitrateMode.VBR:
            logging.warning("Using VBR is not recommended in conjunction "
                            "with the native AAC encoder")
            if not (0.1 <= self.bitrate <= 2.0):
                raise InvalidSettings(
                    "AAC quality factor must be between 0.1 and 2.0")

    def get_quality_args(self) -> Dict[str, str]:
        if self.bitrate_mode == BitrateMode.CBR:
            return {
                "audio_bitrate": f"{self.bitrate}k"
            }
        else:
            return {
                "aq": f"{self.bitrate}"
            }


class FFmpegFdkAacTranscoder(FFmpegTranscoder):
    _encoder = "libfdk-aac"
    _file_extension = ".m4a"

    def verify_quality(self) -> None:
        raise NotImplementedError

    def get_quality_args(self) -> Dict[str, str]:
        raise NotImplementedError


class FFmpegVorbisTranscoder(FFmpegTranscoder):
    _encoder = "libvorbis"
    _file_extension = ".ogg"

    def verify_quality(self) -> None:
        if self.bitrate_mode == BitrateMode.VBR:
            if not (-1.0 <= self.bitrate <= 10.0):
                raise InvalidSettings(
                    "Vorbis quality factor must be between -1.0 and 10.0")

    def get_quality_args(self) -> Dict[str, str]:
        if self.bitrate_mode == BitrateMode.CBR:
            return {
                "audio_bitrate": f"{self.bitrate}k"
            }
        else:
            return {
                "aq": f"{self.bitrate}"
            }


class FFmpegNotFoundException(Exception):
    pass


class InvalidSettings(Exception):
    pass


def autodetect_ffmpeg_path(required_encoder: str = "") -> Optional[Path]:
    """
    Return paths to FFmpeg executable that supports the encoder specified in
    required_encoder

    :param required_encoder: Name of encoder required
    :return: path to FFmpeg executable, or None if no executable satisfies the
    conditions
    """

    available_paths: List[Path] = [
        Path("bin/ffmpeg"),
    ]

    system_ffmpeg = shutil.which("ffmpeg")
    if system_ffmpeg is not None:
        available_paths.append(Path(system_ffmpeg))

    for path in available_paths:
        if not path.exists():
            continue
        proc = subprocess.run([str(path), "-encoders"], capture_output=True)
        available_encoders = proc.stdout.decode()
        if required_encoder not in available_encoders:
            logger.warning("FFmpeg at %s does not support encoder %s, "
                           "ignoring", str(path), required_encoder)
            continue
        logger.info("Found FFmpeg at %s", str(path))
        return path

    return None
