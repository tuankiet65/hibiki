#!/usr/bin/env python3

import logging
from pathlib import Path

from hibiki.database import Database
from hibiki.objects import AudioFormatNotSupported, Track

logger = logging.getLogger(__name__)


class LocalDatabase(Database):
    """
    Class representing a local collection (local here means the collection is
    purely consisted of tracks located on the current machine)
    """

    def add_folder(self, path: Path) -> None:
        """
        Scan |path| for any music files and add them to the collection

        :param path: path to a folder
        :return: None
        """
        for file in path.glob("**/*"):
            if not file.is_file():
                continue
            try:
                logger.debug("Parsing %s", str(file))
                track = Track.load_file(file)

                logger.info("Adding track %s, album %s",
                            track.title,
                            track.album_title or "null")
                self.add_track(track)
            except AudioFormatNotSupported:
                logger.warning("Ignoring unsupported audio file: %s", str(file))
