#!/usr/bin/env python3

import json
import logging
from pathlib import Path
from typing import Any, Dict

from hibiki.database import Database
from hibiki.objects import Album, Track
from hibiki.transcoders import NullTranscoder, Transcoder, TranscodingError
from hibiki.utils import sanitize_path_name

logger = logging.getLogger(__name__)


class RemoteDatabase(Database):
    """
    Class representing a remote collection. A remote collection is based on a
    local collection and usually is a 1:1 copy of the local one, located outside
    of the local collection. For example, a remote collection might represent
    a music player or a mobile device that synchronizes its music collection
    with a computer. Unlike a local collection, a remote collection stores
    track and album information in a database file.
    """

    DATABASE_FILENAME = "database.json"

    def __init__(self, path: Path, transcoder: Transcoder):
        super().__init__()
        self.__path = path
        self.__database_file = path / self.DATABASE_FILENAME
        self.__transcoder = transcoder

        self.load()
        self.verify_tracks()

    def add_track(self, track: Track) -> None:
        """
        Add a track to the collection. Optionally if the track is lossless,
        transcode it using the transcoder specified at initialization

        :param track: The track to be added
        """
        logger.info("Adding track %s", track.title)

        if self.__track_is_lossless(track):
            file_ext = self.__transcoder.file_extension
            transcoder = self.__transcoder
        else:
            file_ext = track.file_path.suffix
            transcoder = NullTranscoder()

        track_file_path = self.__get_track_file_path(track, file_ext)
        assert (not track_file_path.exists())

        try:
            transcoder.transcode(track.file_path, track_file_path)
        except TranscodingError as e:
            logger.exception("Fatal error while transcoding: %s", e.error)

        track.add_remote_info(track_file_path, self.__path)

        super().add_track(track)
        old_album_cover = self._albums[track.album_id].cover_art
        self.__process_album_cover_art(old_album_cover, track.album_id)

    @staticmethod
    def __track_is_lossless(track: Track) -> bool:
        lossless_suffixes = [
            ".flac",  # Free Lossless Audio Codec
            ".tta",  # True Audio
            ".wav",  # Wave
            ".alac",  # Apple Lossless Audio Codec
            ".aiff",  #
            ".ape",  # Monkey's Audio
        ]

        if track.file_path is None:
            return False

        for suffix in lossless_suffixes:
            if track.file_path.suffix.lower() == suffix:
                return True

        return False

    def __get_track_file_path(self, track: Track, file_ext: str) -> Path:
        album_folder_name = sanitize_path_name(
            f"{track.album_title} [{track.album_id[:7]}]"
        )
        album_folder_path = self.__path / album_folder_name

        if not album_folder_path.exists():
            album_folder_path.mkdir()

        track_file_name = sanitize_path_name(
            f"{track.disc:02d}.{track.track_num:02d} - "
            f"{track.title} [{track.id[:7]}]{file_ext}"
        )
        track_file_path = album_folder_path / track_file_name

        return track_file_path

    def __process_album_cover_art(self, old_album_cover, album_id):
        album = self._albums[album_id]

        if old_album_cover != album.cover_art:
            if album.cover_art is None:
                # delete preexisting cover
                pass
            else:
                # save cover
                pass

    def load(self) -> None:
        """
        Load the collection from the database file
        """
        if not self.__database_file.exists():
            return

        with self.__database_file.open("r") as f:
            collection_dict = json.load(f)

        # TODO: do something with collection_dict["transcoder"]

        for album_id, album_dict in collection_dict["albums"].items():
            album = Album.from_dict(album_dict)
            for track in album.tracks:
                super().add_track(track)

    def verify_tracks(self) -> None:
        """

        :return:
        """
        for track_id, track in self.tracks.items():
            if not track.verify_remote_integrity(self.__path):
                logger.warning("Integrity check failed for track %s of album %s"
                               ", removing", track.title, track.album_title)
                self.remove_track(track_id)

    def save(self) -> None:
        """
        Store the collection to a database file
        """
        collection_dict = self.as_dict()
        with self.__database_file.open("w") as f:
            json.dump(collection_dict, f, indent=4)

    def as_dict(self) -> Dict[str, Any]:
        """

        :return:
        """
        with self._lock:
            result = dict()

            result["albums"] = dict()
            for album_id, album in self.albums.items():
                result["albums"][album_id] = album.as_dict(include_remote=True)

            # TODO: implement transcoder.as_dict()
            # result["transcoder"] = self.__transcoder.as_dict()

        return result

    def remove_track(self, track_id: str) -> None:
        """
        Remove a track from the collection

        :param track_id: ID of the track to be removed
        :return:
        """
        pass

    @property
    def path(self) -> Path:
        return self.__path

    @property
    def transcoder(self) -> Transcoder:
        return self.__transcoder
