#!/usr/bin/env python3

import abc
import logging
import threading
from typing import Dict

from hibiki.objects import Album, Track

logger = logging.getLogger(__name__)


class Database(abc.ABC):
    """
    Abstract class representing a music collection.
    """

    def __init__(self):
        """

        """
        self._tracks: Dict[str, Track] = {}
        self._albums: Dict[str, Album] = {}

        # Make this class thread-safe
        self._lock = threading.RLock()

    def add_track(self, track: Track) -> None:
        """
        Add track to collection. Thread safe

        :param track:
        :return:
        """
        with self._lock:
            self._tracks[track.id] = track

            album_id = track.album_id
            if album_id not in self._albums:
                self._albums[album_id] = Album(track.album_title)
            self._albums[album_id].add_track(track)

    def as_dict(self) -> Dict[str, Dict]:
        """
        Return the collection as a dict of primitive types, suitable for
        serialization to formats like JSON/YAML/...

        :return: Dict representing the collection
        """
        with self._lock:
            result = {}
            for track_id, track in self._tracks.items():
                result[track_id] = track.as_dict()
            return result

    def get_track(self, track_id: str) -> Track:
        """
        Return the track with the specified id

        :param track_id: id of the track to be returned
        :return: track matching the id specified
        """
        return self._tracks[track_id]

    @property
    def tracks(self) -> Dict[str, Track]:
        return self._tracks

    @property
    def albums(self) -> Dict[str, Album]:
        return self._albums
